from sklearn.model_selection import train_test_split
from sklearn.neighbors import KNeighborsClassifier
import numpy as np
import cv2 as cv

# Read data using opencv
img = cv.imread('data.png')
gray = cv.cvtColor(img,cv.COLOR_BGR2GRAY)
cells = [np.hsplit(row, 20) for row in np.vsplit(gray, 24)]
x = np.array(cells)
y = np.repeat(np.arange(6), 80)[:,np.newaxis]

# Reshape x to a 2d array
x = x.reshape(-1, 1225).astype(np.float32)
# Flatten y
y = y.flatten()

# Split data and labels into training and testing sets
X_train, X_test, y_train, y_test = train_test_split(x, y, test_size=0.5, random_state=0)

# Train and test model
model = KNeighborsClassifier(n_neighbors=7)
model.fit(X_train, y_train)
print(model.score(X_test, y_test))