# K nearest neighbours
In this project I used OpenCV's and Sklearn's kNN-models  
to make a script that recognizes my handwriting.  

## Prerequisites:
In order to run these scripts, you need the following:  
- numpy
- opencv-python
- sklearn

## Results:
OpenCV kNN accuracy: `71.66%` with `k=1`  
Sklearn kNN accuracy: `90.8%` with `k=7`