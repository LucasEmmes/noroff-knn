import numpy as np
import cv2 as cv
img = cv.imread('data.png')
gray = cv.cvtColor(img,cv.COLOR_BGR2GRAY)

# Now we split the image to 480 cells, each 35x35 size
cells = [np.hsplit(row,20) for row in np.vsplit(gray,24)]

# Make it into a Numpy array: its size will be (20,24,35,35)
x = np.array(cells)

# Now we prepare the training data and test data
train = x[:,:10].reshape(-1,1225).astype(np.float32) # Size = (240,1225)
test = x[:,10:20].reshape(-1,1225).astype(np.float32) # Size = (240,1225)

# Create labels for train and test data
# 0=A, 1=B .. 5=F
k = np.arange(6)
# Since we have 80 of each character and half are used for training, we repeat 40 times
train_labels = np.repeat(k,40)[:,np.newaxis]
test_labels = train_labels.copy()

# Initiate kNN, train it on the training data, then test it with the test data with k=1
knn = cv.ml.KNearest_create()
knn.train(train, cv.ml.ROW_SAMPLE, train_labels)
# Setting k to 1 gave the highest accuracy with my dataset
ret,result,neighbours,dist = knn.findNearest(test,k=1)

# Now we check the accuracy of classification
# For that, compare the result with test_labels and check which are wrong
matches = result==test_labels
correct = np.count_nonzero(matches)
accuracy = correct*100.0/result.size
print( accuracy )